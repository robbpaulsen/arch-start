# arch-start

## Resumen

Este proyecto inicializa un sistema recien instalado de Athena Os, derivada basada en Arch Linux. Actualiza,
instala y configura lo esencial para desarrollo, pentesting o usar el sistema como plataforma para aprender
algun nuevo tema, al terminar la instalacion ya cuenta con:

* Rust y Cargo
* Golang compilador y utilidades
* Suite de comilacion automatizada de CMAKE, MAKE, CLANG y LLVM
* Variables de entorno en compliance con la organizacion "FreeDesktop.org", lo que 
asegura un entorno mas estable y de facil gestion para instalar multiples versiones
de un mismo compilador de los lenguajes previamente mencionados
* Una lista muy completa de Alias para todo tipo de actividad, desde descargar videos
de plataformas populares en la mejor calidad o recolectar reportes para troubleshooting
* Lista aplia de funciones con enfoques variados desde OSINT, fuerza bruta, Bug Bounty,
reconocimiento de red, entre otros

La distribucion de Athena Os es muy ligera y optimizada para programacion/desarrollo,
pentesting, bug bounty, CTF's y cuenta con varios playgrounds para aprender de muchos temas
en auditoria a sistemas, redes y empresas, desde Inyecion SQL, Cross Site Scripting,
Escalada de Privilegios en Windows y Linux.

**Nota:**

La shell por default es zsh para esta configuracion pero tambien todas las configuraciones
que aplique para ZSH tambien ajuste al mismo tiempo la configuracion para Bash, asi que cuenta 
con la misma capacidad y acceso a los mismos recursos.

###### Advertencia

No modificar las variables de entorno configuradas especialmente las que contienen `XDG` al inicio
el sistema y toda aplicacion que se llegaase a instalar tienen como prioridad el guiarse por esas 
variables que contienen `XDG` asi que el solo modificar una puede desencadenar que la terminal
no renderice bien los colores, no poder cambiar de directorio, que las aplicaciones se instalen en 
lugares extranos y sin advertencia al usuario, en especial el cache que pueda genera cada aplicacion,
her3ramienta o utilidad puede multiplicarse por otros lugares simultaneamente. Para cualquier
modificacion consultar:

https://www.freedesktop.org/wiki/
