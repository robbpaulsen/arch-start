#!/usr/bin/env bash

uHome=$(basename /home/*)
. /home/${uHome}/.local/functions/lib/colors/colors.sh

#if [ "$EUID" -ne 0 ]; then
#	echo ""
#	echo -e "\t\n${BIRED}[!]${NC}${BIYELL} Se te olvido el sudo ...\n"
#	tput sgr0
#	exit
#fi


ctrl_c() {
	clear
	echo -e "\t\n${BRED}[!]${NC}${BIYELL} Se interrumpe el programa por CTRL+C ${NC}\n"
	sleep 3
	exit
}
trap ctrl_c INT

sys_rbt() {
	clear
    	echo -e "\t\n${BYELL}[i]${NC}${BIBLU} El Sistema se Reiniciara en 5 Segundos${NC}\n"
    	sleep 5 &&
		systemctl reboot
}

exit_script() {
    clear
    echo -e "\t\n${BBLU}[i]${NC}${BIYELL} Saliendo del Programa ...${NC}\n"
    exit
}

misc() {
    clear
    echo -e "\t\n${BGIBLCK}${BIYELL}[!]${NC}${BGIBLCK}${BIRED} Opcion Invalida, Escoge Otra Opcion ...${NC}"
}

menu() {
	clear
	while true; do
		echo ""
		echo -e "\n$1 ${BGREEN}1 [+]${NC}${BBLU} <OPCION 1>${NC}"
		echo -e "\n$2 ${BGREEN}2 [+]${NC}${BBLU} <OPCION 2>${NC}"
		echo -e "\n$3 ${BGREEN}3 [+]${NC}${BBLU} <OPCION 3>${NC}"
		echo -e "\n$4 ${BGREEN}4 [+]${NC}${BBLU} Salir del Programa${NC}"
		echo -e "\n$5 ${BGREEN}5 [+]${NC}${BBLU} Reiniciar el Sistema${NC}"
		echo ""
		tput sgr 6
		read -p "Selecciona la Opcion deseada: " choice
		tput sgr0
		case ${choice} in
		1)
			;;
		2)
			;;
		3)
			;;
		4)
			clear
			exit_script
			sleep 2
			clear
			;;
		5)
			clear
			sys_rbt
			sleep 2
			clear
			;;
		*)
			clear
			misc
			sleep 2
			clear
			;;
		esac
	done
}
menu
