# Regular Colors
# Reset

NC='\033[0m'           # TEXT RESET

# REGULAR COLORS
BLCK='\033[0;30m'         # BALCK
RED='\033[0;31m'          # RED
GREEN='\033[0;32m'        # GREEN
YELL='\033[0;33m'         # YELLOW
BLU='\033[0;34m'          # BLUE
PURPLE='\033[0;35m'       # PURPLE
CYAN='\033[0;36m'         # CYAN
WHITE='\033[0;37m'        # WHITE

# LIGHT COLORS
LGRAY='\033[00;37m'
LRED='\033[01;31m'
LGREEN='\033[01;32m'
LYELL='\033[01;33m'
LBLU='\033[01;34m'
LPURPLE='\033[01;35m'
LCYAN='\033[01;36m'

# BOLD
BBLCK='\033[1;30m'        # BLACK
BRED='\033[1;31m'         # RED
BGREEN='\033[1;32m'       # GREEN
BYELL='\033[1;33m'        # YELLOW
BBLU='\033[1;34m'         # BLUE
BPURPLE='\033[1;35m'      # PURPLE
BCYAN='\033[1;36m'        # CYAN
BWHITE='\033[1;37m'       # WHITE

# UNDERLINE
UBLCK='\033[4;30m'        # BLACK
URED='\033[4;31m'         # RED
UGREEN='\033[4;32m'       # GREEN
UYELL='\033[4;33m'        # YELLOW
UBLU='\033[4;34m'         # BLUE
UPURPLE='\033[4;35m'      # PURPLE
UCYAN='\033[4;36m'        # CYAN
UWHITE='\033[4;37m'       # WHITE

# BACKGROUND
BGBLCK='\033[40m'         # BLACK
BGRED='\033[41m'          # RED
BGGREEN='\033[42m'        # GREEN
BGYELL='\033[43m'         # YELLOW
BGBLU='\033[44m'          # BLUE
BGPURPLE='\033[45m'       # PURPLE
BGCYAN='\033[46m'         # CYAN
BGWHITE='\033[47m'        # WHITE

# HIGH INTENSITY
IBLCK='\033[0;90m'        # BLACK
IRED='\033[0;91m'         # RED
IGREEN='\033[0;92m'       # GREEN
IYELL='\033[0;93m'        # YELLOW
IBLU='\033[0;94m'         # BLUE
IPURPLE='\033[0;95m'      # PURPLE
ICYAN='\033[0;96m'        # CYAN
IWHITE='\033[0;97m'       # WHITE

# BOLD HIGH INTENSITY
BIBLCK='\033[1;90m'       # BLACK
BIRED='\033[1;91m'        # RED
BIGREEN='\033[1;92m'      # GREEN
BIYELL='\033[1;93m'       # YELLOW
BIBLU='\033[1;94m'        # BLU
BIPURPLE='\033[1;95m'     # PURPLE
BICYAN='\033[1;96m'       # CYAN
BIWHITE='\033[1;97m'      # WHITE

# HIGH INTENSITY BACKGROUNDS
BGIBLCK='\033[0;100m'    # BLACK
BGIRED='\033[0;101m'     # RED
BGIGREEN='\033[0;102m'   # GREEN
BGIYELL='\033[0;103m'    # YELLOW
BGIBLU='\033[0;104m'     # BLU
BGIPURPLE='\033[0;105m'  # PURPLE
BGICYAN='\033[0;106m'    # CYAN
BGIWHITE='\033[0;107m'   # WHITE

randomcolor() {
    case ${1} in
        simple)
            echo -ne "\e[3$(( ${RANDOM} * 6 / 32767 + 1 ))m"
        ;;
        cool)
            local bold=$(( ${RANDOM} % 2 ))
            local code=$(( 30 + ${RANDOM} % 8 ))
            printf "%d;%d\n" $bold $code
        ;;
    esac
}

# lolbash is a function that offers random colors similar to lolcat for every character(s)
# Usage: echo -e "$(lolbash) hello${reset}"

lolbash() {
	sentence="$*"
	for (( i=0; i<${#sentence}; i++ )); do
	    printf "\e[%sm%c" "$(random_colour)" "${sentence:i:1}"
	done
	echo -e '\e[0m'
}

# Status
Correct="${NC}${GREEN} ✔ ${NC}${GREEN}${NC}"
Incorrect="${NC}${RED} ✘${NC}${RED}${NC}"
