RED="$(tput setaf 1)"
GRN="$(tput setaf 2)"
YELL="$(tput setaf 3)"
BLU="$(tput setaf 4)"
MAG="$(tput setaf 5)"
CYN="$(tput setaf 6)"
LTRED="$(tput setaf 9)"
LTGRN="$(tput setaf 10)"
LTYELL="$(tput setaf 11)"
LTBLU="$(tput setaf 12)"
LTMAG="$(tput setaf 13)"
LTCYN="$(tput setaf 14)"
#
BLRED=${BOLD}${LTRED}
BLYELL=${BOLD}${LTYELL}
BYELL=${BOLD}${YELL}
BLCYN=${BOLD}${LTCYN}
BCYN=${BOLD}${CYN}
#
BGBLU="$(tput setab 4)"
BGYELL="$(tput setab 3)"
BGLYELL="$(tput setab 11)"
#
ULINE="$(tput smul)"
NULINE="$(tput rmul)"
SOMODE="$(tput smso)"
NSOMODE="$(tput rmso)"
BOLD="$(tput bold)"
RESET="$(tput sgr0)"
