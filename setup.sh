#!/usr/bin/env bash

# ---------------------------------------------- Libraries ----------------------------------------------- #

. ~+/lib/colors/colors.sh

# ---------------------------------------------- Variables ----------------------------------------------- #

uHome="$(basename /home/*)"

# ---------------------------------------------- Funciones ----------------------------------------------- #
clear

ctrl_c() {
  exit
}
trap ctrl_c INT

echo
echo -e "\t\n${BBLU}[+]${NC}${BGREEN} No ejecutar este script usando SUDO , en el transcurso de su ejecucion pedira el password para sudo pero no hacerlo antes\nPresiona CTRL + C,  de lo contrario tendras 5 segundos para detenerlo ...${NC}\n"
echo
sleep 5

chexe() {
  chmod u+x ~+/assets/*.sh
}

startInstaller() {
  bash ~+/assets/install-1.sh
}

chexe &&
  startInstaller
