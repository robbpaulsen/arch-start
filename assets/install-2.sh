#!/usr/bin/env bash

# ---------------------------------------------- Libraries ----------------------------------------------- #

. ~+/lib/colors/colors.sh

# ---------------------------------------------- Variables ----------------------------------------------- #

uHome=$(basename /home/*)

# ---------------------------------------------- Funciones ----------------------------------------------- #
clear

echo -n "\e[34m                  ▄
                 ▄█▄
                ▄███▄
               ▄█████▄
              ▄███████▄
             ▄ ▀▀██████▄
            ▄██▄▄ ▀█████▄
           ▄█████████████▄
          ▄███████████████▄
         ▄█████████████████▄
        ▄███████████████████▄
       ▄█████████▀▀▀▀████████▄
      ▄████████▀      ▀███████▄
     ▄█████████        ████▀▀██▄
    ▄██████████        █████▄▄▄
   ▄██████████▀        ▀█████████▄
  ▄██████▀▀▀              ▀▀██████▄
 ▄███▀▀                       ▀▀███▄
▄▀▀                               ▀▀▄\e[0m"

ctrl_c() {
  exit
}
trap ctrl_c INT

echo
echo -e "\t\n${BBLU}[+]${NC}${BGREEN} No ejecutar como administrador/sudo Se instalaran utilidades de Rust ...${NC}\n"
echo
sleep 3

rcargoInstaller() {
  echo
  while read -r line; do
    echo -e "\n${BYELL}[i]${NC}${BGREEN} Instalando ${line}${NC}\n"
    cargo install ${line} 2>/dev/null
  done <~+/assets/rcargo.lst
  echo
  echo -e "\t\n${BYELL}[i]${NC}${BCYAN} Finalizan las instalaciones${NC}\n"
  echo
  sleep 2
}

rcargoInstaller
