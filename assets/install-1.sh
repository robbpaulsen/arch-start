#!/usr/bin/env bash

# ---------------------------------------------- Libraries ----------------------------------------------- #

. ~+/lib/colors/colors.sh

# ---------------------------------------------- Variables ----------------------------------------------- #
export XDG_DESKTOP_DIR=${HOME}/Desktop
export XDG_DOWNLOAD_DIR=${HOME}/Downloads
export XDG_TEMPLATES_DIR=${HOME}/Templates
export XDG_PUBLICSHARE_DIR=${HOME}/Public
export XDG_DOCUMENTS_DIR=${HOME}/Documents
export XDG_MUSIC_DIR=${HOME}/Music
export XDG_PICTURES_DIR=${HOME}/Pictures
export XDG_VIDEOS_DIR=${HOME}/Videos
export XDG_CACHE_HOME=${HOME}/.cache
export XDG_CONFIG_HOME=${HOME}/.config
export XDG_DATA_HOME=${HOME}/.local/share
export XDG_STATE_HOME=${HOME}/.local/state
export XCURSOR_PATH=/usr/share/icons:${XDG_DATA_HOME}/icons
export RUSTUP_HOME=${HOME}/.rustup
export CARGO_HOME=${HOME}/.cargo
export BIN_DIR=${BIN_DIR}:-${CARGO_DEFAULT_BIN}
export CARGO_DEFAULT_BIN=${CARGO_HOME}/bin
export PIPX_HOME=${XDG_DATA_HOME}/pipx/venvs
export PIPX_BIN_DIR=${XDG_DATA_HOME}/pipx/bin
export PIPX_MAN_DIR=${XDG_DATA_HOME}/man
export GOROOT=/usr/lib/go
export GOPATH=${HOME}/go
export PATH=${PATH}:${GOROOT}/bin:${GOPATH}/bin
export GO111MODULE=on
export SHELL=/usr/bin/zsh
export TERM=xterm-256color
export COLORTERM=truecolor
export EDITOR=/usr/bin/nvim
export VISUAL=/usr/bin/nvim
export BROWSER=firefox
export PATH=$PATH:${HOME}/.cargo/bin
export PATH="${HOME}/neovim/bin:${PATH}"
export TERMINFO=${HOME}/.local/share/terminfo
export TERMINFO_DIRS=${HOME}/.local/share/terminfo:/usr/share/terminfo
export ICEAUTHORITY=${XDG_CACHE_HOME}/ICEauthority
[ -d ${HOME}/.local/bin ] && export PATH=${PATH}:${HOME}/.local/bin
export uHome="$(basename /home/*)"
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

# ---------------------------------------------- Funciones ----------------------------------------------- #
clear

echo -n "\e[34m                  ▄
                 ▄█▄
                ▄███▄
               ▄█████▄
              ▄███████▄
             ▄ ▀▀██████▄
            ▄██▄▄ ▀█████▄
           ▄█████████████▄
          ▄███████████████▄
         ▄█████████████████▄
        ▄███████████████████▄
       ▄█████████▀▀▀▀████████▄
      ▄████████▀      ▀███████▄
     ▄█████████        ████▀▀██▄
    ▄██████████        █████▄▄▄
   ▄██████████▀        ▀█████████▄
  ▄██████▀▀▀              ▀▀██████▄
 ▄███▀▀                       ▀▀███▄
▄▀▀                               ▀▀▄\e[0m"

ctrl_c() {
  exit
}
trap ctrl_c INT

echo
echo -e "\t\n${BBLU}[!]${NC}${BGREEN} No ejecutar este script usando SUDO , en el transcurso de su ejecucion pedira el password para sudo pero no hacerlo antes\nPresiona CTRL + C,  de lo contrario tendras 5 segundos para detenerlo ...${NC}\n"
echo
sleep 5

usrGrps() {
  echo
  echo -e "\n${BLU}[i]${NC}${BGREEN} Agregando los grupos requeridos ...${NC}\n"
  sudo usermod -aG adm audio colord developer dhcpcd disk docker git http kmem kvm lightdm locate lock log lp mem network nm-openvpn openvpn power rfkill tor utmp uuidd video wheel $(whoami) &>/dev/null
  echo
}

devLayout() {
  echo
  echo -e "\n${BLU}[i]${NC}${BGREEN} Generando Directorios de Trabajo ...${NC}\n"
  echo
  mkdir -p /home/${uHome}/Projects
  mkdir -p /home/${uHome}/Downloads/git-repos
  mkdir -p /home/${uHome}/.local
  mkdir -p /home/${uHome}/.local/share
  mkdir -p /home/${uHome}/.local/state
  mkdir -p /home/${uHome}/.local/bin
  sleep 2
}

confsPlcmnt() {
  clear
  rm -rf /home/${uHome}/.bashrc &>/dev/null &&
    rm -rf /home/${uHome}/.bash_profile &>/dev/null &&
    rm -rf /home/${uHome}/.bash_aliases &>/dev/null &&
    rm -rf /home/${uHome}/.zshrc &>/dev/null &&
    rm -rf /home/${uHome}/.zprofile &>/dev/null &&
    rm -rf /home/${uHome}/.config/kitty &>/dev/null &&
    rm -rf /home/${uHome}/.config/nvim &>/dev/null &&
    rsync -rzvhP ~+/confs/config/* /home/${uHome}/.config/ &&
    rsync -zvhP ~+/confs/home/zshrc /home/${uHome}/.zshrc &&
    rsync -zvhP ~+/confs/home/bash_aliases /home/${uHome}/.bash_aliases &&
    rsync -zvhP ~+/confs/home/bashrc /home/${uHome}/.bashrc &&
    rsync -zvhP ~+/confs/home/bash_profile /home/${uHome}/.bash_profile &&
    rsync -zvhP ~+/confs/home/functions.sh /home/${uHome}/.functions.sh &&
    rsync -zvhP ~+/confs/home/zprofile /home/${uHome}/.zprofile &&
    rsync -zvhP ~+/confs/home/functions.zsh /home/${uHome}/.functions.zsh &&
    rsync -zvhP ~+/confs/home/key-bindings.zsh /home/${uHome}/.key-bindings.zsh &&
    rsync -zvhP ~+/confs/home/tmux.conf /home/${uHome}/.tmux.conf &&
    rsync -rzvhP ~+/confs/home/local/bin/* /home/${uHome}/.local/bin/ &&
    rsync -rzvhP ~+/confs/home/local/share/* /home/${uHome}/.local/share/ &&
    sudo fc-cache -v -f
}

nvmInstaller() {
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.1/install.sh | bash &&
    nvm install --lts &&
    nvm use --lts
}

archInstaller() {
  echo
  sudo pacman -Syyu --needed --noconfirm &&
    sleep 2
  while read -r line; do
    echo -e "\n${YELL}[i]${NC}${GREEN} Instalando ${line}${NC}\n"
    sudo pacman -S --needed --noconfirm ${line} 2>/dev/null
  done <assets/archpkg.lst
  sudo updatedb
  echo
  echo -e "\t\n${BYELL}[i]${NC}${BCYAN} Finalizan las instalaciones${NC}\n"
  echo
  sleep 2
}

usrGrps &&
  devLayout &&
  confsPlcmnt &&
  nvmInstaller &&
  archInstaller
