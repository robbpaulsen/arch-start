# User Functions

## Zoxide
_z_cd() {
  cd "$@" || return "$?"

  if [ "$_ZO_ECHO" = "1" ]; then
    echo "$PWD"
  fi
}

z() {
  if [ "$#" -eq 0 ]; then
    _z_cd ~
  elif [ "$#" -eq 1 ] && [ "$1" = '-' ]; then
    if [ -n "$OLDPWD" ]; then
      _z_cd "$OLDPWD"
    else
      echo 'zoxide: $OLDPWD is not set'
      return 1
    fi
  else
    _zoxide_result="$(zoxide query -- "$@")" && _z_cd "$_zoxide_result"
  fi
}

zi() {
  _zoxide_result="$(zoxide query -i -- "$@")" && _z_cd "$_zoxide_result"
}

zri() {
  _zoxide_result="$(zoxide query -i -- "$@")" && zoxide remove "$_zoxide_result"
}

_zoxide_hook() {
  if [ -z "${_ZO_PWD}" ]; then
    _ZO_PWD="${PWD}"
  elif [ "${_ZO_PWD}" != "${PWD}" ]; then
    _ZO_PWD="${PWD}"
    zoxide add "$(pwd -L)"
  fi
}

# Useful Functions

cdd() {
  cd "$(lsd -d -- */ | fzf)" || echo "Invalid directory"
}

# This script depends on pushd. It works better with autopush enabled in ZSH
recent_dirs() {
  escaped_home=$(echo $HOME | sed 's/\//\\\//g')
  selected=$(dirs -p | sort -u | fzf)
  cd "$(echo "$selected" | sed "s/\~/$escaped_home/")" || echo "Invalid directory"
}

# Create common working dirs for a CTF
mkt() {
 mkdir {nmap,content,exploits,scripts}
}

# Extract the ports from the nmap scan
extractPorts() {
  ports="$(/usr/bin/cat $1 | /usr/bin/grep -oP '\d{1,5}/open' | awk '{print $1}' FS='/' | xargs | tr ' ' ',')"
  ip_address="$(/usr/bin/cat $1 | /usr/bin/grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u | head -n 1)"
  echo -e "\n[*] Extracting information...\n" >extractPorts.tmp
  echo -e "\t[*] IP Address: $ip_address" >>extractPorts.tmp
  echo -e "\t[*] Open ports: $ports\n" >>extractPorts.tmp
  echo $ports | tr -d '\n' | xclip -sel clip
  echo -e "[*] Ports copied to clipboard\n" >>extractPorts.tmp
  /usr/bin/cat extractPorts.tmp
  rm extractPorts.tmp
}

# Manpages with colors
man() {
    env \
    LESS_TERMCAP_mb=$'\e[01;31m' \
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
  man "$@"
}

# Scrub cache
rmk() {
  scrub -p dod $1
  shred -zun 10 -v $1
}

# find the most common/repeated word on a file
mode() {
  if [[ $# -ne 1 ]]; then
    echo 'find the most common item in file'
    echo 'EXAMPLE: mode <FILE>'
  else
    LC_ALL=C sort -T ./ $1 | uniq -c | LC_ALL=C sort -T ./ -rn
  fi
}

# unique sort file
usort() {
  if [[ $# -ne 1 ]]; then
    echo 'unique sort file inplace'
    echo 'EXAMPLE: usort <FILE>'
  else
    LC_ALL=C sort -u $1 -T ./ -o $1
  fi
}

# Get the display manager being used
get_display() {
  dispmngr="$(/usr/bin/grep 'ExecStart' /etc/systemd/system/display-manager.service | tr '/' ' ' | awk '{ print $NF }')"
  echo -e "\n[i] This is your display manager: $dispmngr\n"
}

# List the dependencies of a package with pacman package manager
pacman-deps() {
  LC_ALL=C.UTF-8 pacman -Si $1 $2 $3 $4 $5 $6 |
    awk -F'[:<=>]' '/^Depends/ {print $2}' |
    xargs -n1 |
    sort -ub
}

# retrieve SELinux enforcing status
function genf() {
  getenforce
}

# set SELinux enforce status to Permissive
set_free() {
  enfstat="$(getenforce)"
  sudo setenforce 0 &&
    echo $enfstat
}

# check your weather putting city as parameter
weather() {
  curl "https://v2.wttr.in/$1"
}

# cheat-sheet in terminal
cheat() {
  curl "https://cht.sh/$1"
}

# # ex = EXtractor for all kinds of archives
# # usage: ex <file>
ex() {
  if [ -f $1 ]; then
    case $1 in
    *.tar.bz2) tar xjf $1 ;;
    *.tar.gz) tar xzf $1 ;;
    *.bz2) bunzip2 $1 ;;
    *.rar) unrar x $1 ;;
    *.gz) gunzip $1 ;;
    *.tar) tar xf $1 ;;
    *.tbz2) tar xjf $1 ;;
    *.tgz) tar xzf $1 ;;
    *.zip) unzip $1 ;;
    *.Z) uncompress $1 ;;
    *.7z) 7z x $1 ;;
    *.deb) ar x $1 ;;
    *.tar.xz) tar xf $1 ;;
    *.tar.zst) unzstd $1 ;;
    *) echo ''$1' cannot be extracted via ex()' ;;
    esac
  else
    echo ''$1' is not a valid file'
  fi
}

# Make a list of all words in a line by line list without dupli/usr/bin/cates
kln_lns() {
  awk '{ if (!seen[$0]++) print }' $1
}

test_colors() {
  awk 'BEGIN{
  s="/\\/\\/\\/\\/\\"; s=s s s s s s s s;
  for (colnum = 0; colnum<77; colnum++) {
    r = 255-(colnum*255/76);
    g = (colnum*510/76);
    b = (colnum*255/76);
    if (g>255) g = 510-g;
      printf "\033[48;2;%d;%d;%dm", r,g,b;
      printf "\033[38;2;%d;%d;%dm", 255-r,255-g,255-b;
      printf "%s\033[0m", substr(s,colnum+1,1);
    }
    printf "\n";
  }'
}

# Extract Phone numbers form a string of text or large file
extract_phone_numbers() {
  local input="$1"
  local phone_numbers=$(echo "$input" | /usr/bin/grep -Eo '[0-9]{2} ?[0-9]{4} ?[0-9]{4}' | tr -d ' ')
  echo "${phone_numbers}"
}

# Extract email addresses from a string of text or large file
extract_emails() {
  local input="$1"
  local regex='\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}\b'
  /usr/bin/grep -Eo ${regex} ${input}
}

# Extract only the IPv4 Address from a string of text or large file
extract_ips() {
  local input="$1"
  local regex='\b([0-9]{1,3}\.){3}[0-9]{1,3}\b'
  /usr/bin/grep -Eo ${regex} ${input}
}

extract_ips_two() {
  local input="$1"
  local regex='(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)'
  /usr/bin/grep -Eo ${regex} ${input}
}

# Extract the IPv4 Address and Port Number from a string of text or large file
extract_ip_port() {
  local input="$1"
  local regex='\b([0-9]{1,3}\.){3}[0-9]{1,3}:([0-9]{5})\b'
  /usr/bin/grep -Eo ${regex} ${input}
}

# Extract Media Access Control Addresses from a string of text or large file
extract_mac_addresses() {
  local input="$1"
  local regex='\b([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})\b'
  /usr/bin/grep --color=auto -Eo ${regex} ${input}
}

# Extract IPv6 Addresses from a string of text or large file
extract_ipv6() {
  input="$1"
  ipv6_regex='\b([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4}\b'
  /usr/bin/grep --color=auto -Eo ${ipv6_regex} ${input}
}

extract_ipv6_one() {
  input="$1"
  ipv6_regex='\b(:[0-9A-Fa-f]{1,4}:){1,7}[0-9A-Fa-f]{1,4}\b'
  /usr/bin/grep --color=auto -Eo ${ipv6_regex} ${input}
}

extract_ipv6_two() {
  input="$1"
  ipv6_regex='\b(([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4}|([0-9A-Fa-f]{1,4}:){1,7}:)\b'
  /usr/bin/grep --color=auto -Eo ${ipv6_regex} ${input}
}

extract_ipv6_three() {
  input="$1"
  ipv6_regex='\b(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:))\b'
  /usr/bin/grep --color=auto -Eo ${ipv6_regex} ${input}
}

# Display your Public IP
pubIP() {
  local site=ifconfig.me
  local CustomCommand=$(/usr/bin/curl ${site})
  tput setaf 3
  echo -e "\t\n[i] Direccion: ${CustomCommand}\n"
  tput sgr0
}

# Display a Matrix Like digital show on the terminal screen
cDaMatrix() {
  echo -e "\e[1;40m"
  clear
  while :; do
    echo $LINES $COLUMNS $(($RANDOM % $COLUMNS)) $(($RANDOM % 72))
    sleep 0.05
  done |
    /usr/bin/awk '{ letters="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#$%^&*()"; c=$4; letter=substr(letters,c,1);a[$3]=0;for (x in a) {o=a[x];a[x]=a[x]+1; printf "\033[%s;%sH\033[2;32m%s",o,x,letter; printf "\033[%s;%sH\033[1;37m%s\033[0;0H",a[x],x,letter;if (a[x] >= $1) { a[x]=0; } }'
}

# Release and Renew your current IPv4 Address
reNewIP() {
  /usr/sbin/dhclient -r &&
    /usr/sbin/dhclient
}

# Determine if system is 32 or 64 bits
getOsArchType() {
  /usr/bin/cat /proc/cpuinfo |
    /usr/bin/grep -Gq "flags.* lm " &&
    tput setaf 3
  echo -e '\t\n[i] 64bit\n' ||
    echo -e '\t\n[i] 32bit\n'
  tput sgr0
}

# Monitor disk i/o
monDrive() {
  /usr/bin/iostat -x 1
}

# Disk usage summary by directory
driveUsg() {
  /usr/bin/du -chs /*
}

# Disk usage summary by directory, alternate (sometimes more effective)
drvSummary() {
  /usr/bin/du -h / |
    /usr/bin/grep '[0-9\.]\+G'
  /usr/bin/du -h / |
    /usr/bin/grep '^\s*[0-9\.]\+G' # only directories larger than 1.0 GB
  /usr/bin/du -ch -d 1 |
    /usr/bin/sort -hr # uses util ncdu
}

# Find average size of all files on the entire drive (/)
avrFSize() {
  /usr/bin/find / -type f -print0 |
    /usr/bin/xargs -0 ls -l |
    /usr/bin/gawk '{sum += $5; n++;} END {print "Total Size: " sum/1024/1024 " MB : Avg Size: " sum/n/1024 " KB : Total Files: " n ;}'
}

# Verify no non-root users have UID 0 (root)
verifyUsrs() {
  /usr/bin/awk -F: '($3 == "0") {print}' /etc/passwd
}

# Useful tools for watching NIC traffic/throughput
monNicTraffic() {
  /usr/bin/vnstat -l -i eth1
  /usr/bin/iptrafz
  /usr/bin/ntop # Self-contained webGUI traffic monitor, with graphs and detailed tables. Runs by default on port 3000
}

# Add static route for a specific host
addStRoute() {
  /usr/bin/route add -host 192.168.1.83 gw 172.16.1.1
}

# Add static route for a specific subnet
addStRouteSub() {
  /usr/bin/route add -net 192.168.1.0/24 gw 172.16.1.1
}

# Add static route for a specific subnet and specify a metric
#route add -net 192.168.1.0/24 gw 172.16.1.1 3

# RHEL/CentOS: Show all installed packages and the date of last update
showRPMP() {
  /usr/bin/rpm -qa --qf '%{INSTALLTIME} %-40{NAME} %{INSTALLTIME:date}\n' |
    /usr/bin/sort -n |
    /usr/bin/cut -d' ' -f2-
}

function generate_regex() {
  local email=$1
  local regex="^$email$"
  echo "$regex"
}

extract_email_pass() {
  input="$1"
  regex='\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}:[^&]+\b'
  /usr/bin/grep --color=auto -Eo ${regex} ${input}
}

extract_passwords() {
  input="$1"
  regex='(P|p)assw(d|D|ord|ORD)(:|=)[^&]+'

  /usr/bin/grep --color=auto -aioE ${regex} ${input}
}

# Extract hashes
#Extract md5 hashes ({32}), sha1 ({40}), sha256({64}), sha512({128})
extract_md5() {
  input="$1"
  input2="$2"
  regex1='(^|[^a-fA-F0-9])[a-fA-F0-9]{32}([^a-fA-F0-9]|$)'
  regex2='[a-fA-F0-9]{32}'
  /usr/bin/grep -Eo ${regex1} ${input1} |
    /usr/bin/grep -Eo ${regex2} ${input2}
}

# Do you trust all the website's you visit?, pull all the Certifi/usr/bin/cates that are stored on othe system
show_certs() {
  awk -v cmd='openssl x509 -noout -subject' '/BEGIN/{close(cmd)};{ print | cmd }' </etc/ssl/certs/ca-certifi/usr/bin/cates.crt
}

# Get the cabinet Average Fan Spedd
fan_speed() {
  speed="$(sensors | /usr/bin/grep fan2 | awk '{print $2; exit}')"

  if [ "$speed" != "" ]; then
    speed_round=$(echo "$speed/1000" | bc -l | LANG=C xargs printf "%.1f\n")
    echo "# $speed_round"
  else
    echo "#"
  fi
}

wless_down() {
  alias ip='sudo /usr/sbin/ip'
  wiface=$(iw dev | /usr/bin/grep "Interface " | xargs | cut -d " " -f 2)

  ip link set $wiface down
}

wless_rmac() {
  alias macchanger='sudo /bin/macchanger'
  wiface=$(iw dev | /usr/bin/grep "Interface " | xargs | cut -d " " -f 2)

  sudo macchanger -a $wiface
}

wless_omac() {
  alias macchanger='sudo /bin/macchanger'
  wiface=$(iw dev | /usr/bin/grep "Interface " | xargs | cut -d " " -f 2)

  sudo macchanger -p $wiface
}

wless_mon() {
  alias iw='sudo /usr/sbin/iw'
  wiface=$(iw dev | /usr/bin/grep "Interface " | xargs | cut -d " " -f 2)

  iw dev $wiface set type monitor
}

wless_up() {
  alias ip='sudo /usr/sbin/ip'
  wiface=$(iw dev | /usr/bin/grep "Interface " | xargs | cut -d " " -f 2)

  ip link set $wiface up
}

# Unblock any NIC inside RFKILL's prision
wless_unblock() {
  sudo rfkill unblock all
}

# Inplace file/files Archiver/backup, and will name it with the date
tar_pressed() {
  DATE=($(date +%s%d%m%Y))
  tar -zcf backup-${DATE} -C $1 $2 # orden de los archivos "-zcf backup-etc es el nombre del comprimmido que se creara , -C es el directorio base de donde sincronizara y el siguiente argumto es el directorio destino"
}

# Extract Emails from a big chunky File
eml_extractor() {
  /usr/bin/grep -HiEhr -o -e "\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}\b:...*" |
    /usr/bin/grep '\S'
}

# Decode ROT 13 strings
rot13() {
  if [ $# -eq 0 ]; then
    tr '[a-m][n-z][A-M][N-Z]' '[n-z][a-m][N-Z][A-M]'
  else
    echo $* | tr '[a-m][n-z][A-M][N-Z]' '[n-z][a-m][N-Z][A-M]'
  fi
}

# Get the Distribution installed on System
get_distro() {
  /usr/bin/cat /etc/os-release | tr '"' " " | /usr/bin/grep -E "NAME" | head -n 1 | awk -F " " '{ print $2 }'
}

# Pull CPU Brand/Model Information
get_cpu() {
  awk -F '\\s*: | @' '/model name|Hardware|Processor|^cpu model|chip type|^cpu type/ { cpu=$2; if ($1 == "Hardware") exit } END { print cpu }' /proc/cpuinfo
}

# Trim leading and trailing spaces (for scripts)
trim() {
  local var=$@
  var="${var#"${var%%[![:space:]]*}"}" # remove leading whitespace characters
  var="${var%"${var##*[![:space:]]}"}" # remove trailing whitespace characters
  echo -n "$var"
}

# Pull the GPU Information and Model
get_gpu() {
  lspci -mm | awk -F '\"|\" \"|\\(' '/"Display|"3D|"VGA/ {a[$0] = $1 " " $3 " " $4} END {for(i in a) {if(!seen[a[i]]++) print a[i]}}' | awk -F " " '{ print $2,$3,$4,$5,$6,$7 }' | sed 's/\[//g' | sed 's/\]//g'
}

# Downlaod an entire YT Playlist which will create a directory containing the name of playlist and all videos on chronological order
yt_playlist() {
  yt-dlp -o "%(playlist)s/%(playlist_index)s" -f "bv+ba/b" "$1"
}

# Render markdown file with Glow
glowit() {
  <"$1" | glow --config
}

# Render markdown file with Inlyne a browserless and independent Markdown renderer
show_it() {
  inlyne "$1"
}

# Search DNF installed app DB for a app/pkg
app_search() {
  dnf list --installed | /usr/bin/grep -iE "$1"
}

# Take control of your services and start your online presence manually
net_up() {
  sudo systemctl start NetworkManager.service
}

# Take control of your services and start your wifi online presence manually
wlannet_up() {
  sudo systemctl start wpa_supplicant.service NetworkManager.service
}

# Take control of your services and stop your wifi presence manually
wlannet_down() {
  sudo systemctl stop wpa_supplicant.service NetworkManager.service
}

# Convert HTML file to Markdown
html2mdown() {
  pandoc -f html -t markdown -i "$1" -o "$2".md
}

# Convert Markdown file to HTML
mdown2html() {
  pandoc -f markdown -t html -i "$1" -o "$2".html
}

# Convert Markdown file to PDF
mdown2pdf() {
  pandoc -f markdown -t pdf -i "$1" -o "$2".pdf
}

settarget() {
  ip_address=$1
  machine_name=$2
  echo "$ip_address $machine_name" >${XDG_CONFIG_HOME}/bspwm/scripts/target
}

cleartarget() {
  echo '' >$HOME/.config/bspwm/scripts/target
}

#Extract valid MySQL-Old hashes
extract_mysql_old_hashes() {
  input="$1"
  regex='[0-7][0-9a-f]{7}[0-7][0-9a-f]{7}'
  /usr/bin/grep --color=auto -Eo ${regex} ${input}
}

#Extract blowfish hashes
extract_blowfish_hashes() {
  input="$1"
  regex='2a\$\08\$(.){75}'
  /usr/bin/grep --color=auto -Eo ${regex} ${input}
}

randomcolor() {
  case ${1} in
  simple)
    echo -ne "\e[3$((${RANDOM} * 6 / 32767 + 1))m"
    ;;
  cool)
    local bold=$((${RANDOM} % 2))
    local code=$((30 + ${RANDOM} % 8))
    printf "%d;%d\n" $bold $code
    ;;
  esac
}

lolbash() {
  sentence="$*"
  for ((i = 0; i < ${#sentence}; i++)); do
    printf "\e[%sm%c" "$(random_colour)" "${sentence:i:1}"
  done
  echo -e '\e[0m'
}

get_100rate() {
  input="${1}"
  regex='"link": "([^"]+)", "rate": "%100.0"'
  /usr/bin/grep --color=always -Eaio ${regex} ${input} | sed 's/"link": "//g; s/", "rate": "%100.0"//g'
}

##Extract Joomla hashes
#  input="$1"
#  regex='([0-9a-zA-Z]{32}):(w{16,32})'
#
##Extract VBulletin hashes
#  input="$1"
#  regex='([0-9a-zA-Z]{32}):(S{3,32})'
#
##Extraxt phpBB3-MD5
#  input="$1"
#  regex='$H$S{31}'
#
##Extract Wordpress-MD5
#  input="$1"
#  regex='$P$S{31}'
#
##Extract Drupal 7
#  input="$1"
#  input="$1"
#'$S$S{52}'
#
##Extract old Unix-md5
#  input="$1"
#  input="$1"
#'$1$w{8}S{22}'
#
##Extract md5-apr1
#  input="$1"
#  input="$1"
#'$apr1$w{8}S{22}'
#
##Extract sha512crypt, SHA512(Unix)
#  input="$1"
#  input="$1"
#'$6$w{8}S{86}'

buffer_clean(){
  free -h && sudo sh -c 'echo 1 >  /proc/sys/vm/drop_caches' && free -h
}

function cu {
    local count=$1
    if [ -z "${count}" ]; then
        count=1
    fi
    local path=""
    for i in $(seq 1 ${count}); do
        path="${path}../"
    done
    cd $path
}


# Open all modified files in vim tabs
function vimod {
    vim -p $(git status -suall | awk '{print $2}')
}

# Open files modified in a git commit in vim tabs; defaults to HEAD. Pop it in your .bashrc
# Examples:
#     virev 49808d5
#     virev HEAD~3
function virev {
    commit=$1
    if [ -z "${commit}" ]; then
      commit="HEAD"
    fi
    rootdir=$(git rev-parse --show-toplevel)
    sourceFiles=$(git show --name-only --pretty="format:" ${commit} | grep -v '^$')
    toOpen=""
    for file in ${sourceFiles}; do
      file="${rootdir}/${file}"
      if [ -e "${file}" ]; then
        toOpen="${toOpen} ${file}"
      fi
    done
    if [ -z "${toOpen}" ]; then
      echo "No files were modified in ${commit}"
      return 1
    fi
    vim -p ${toOpen}
}

# 'Safe' version of __git_ps1 to avoid errors on systems that don't have it
function gitPrompt {
  command -v __git_ps1 > /dev/null && __git_ps1 " (%s)"
}



#-----------------------------------------------------------------------------------------------------------

function commands() {
  echo ""
  tput setaf 6
  echo "================================ ALL COMMANDS AVAILABLE ============================================="
  echo ""
  echo "weather [city]                         - Check weather"
  echo "cdd                                    - Cd into dir with fzf"
  echo "recent_dirs                            - View most recent accesed dirs"
  echo "mkt                                    - Create a tree layout for pentest workflow"
  echo "extractPorts    [file]                 - Extract the discovered ports from an Nmap file"
  echo "test_colors                            - Test colors #3"
  echo "mode            [file]                 - Find the most common/repeated word on a file"
  echo "usort           [file]                 - in place sort the order of a file with many lines"
  echo "get_display                            - Get the name of the Display Manager"
  echo "cheat           [Command]              - Consult examples for a given command to cheat.sh site"
  echo "ex              [file]                 - EXtractor for all kind of archives"
  echo "kln_lns         [file]                 - Print unsorted unique lines from a list/text file"
  echo "pacman-deps [package 1 ...]            - Print th edependencies oa group of packages on arch linux"
  echo "extract_phone_numbers [string]         - Extract the phone numbers from a large string of text"
  echo "rmk                                    - Scrub and Shred the cache for better performance"
  echo "extract_ips [STRING or FILE]           - Extract all the IPv4's of a given File or String"
  echo "extract_ip_port [STRING or FILE]       - Extract all the IPv4'a and Ports of a given File or String"
  echo "extract_ipv6 [STRING or FILE]          - Extract all the IPv6's of a given File or String"
  echo "extract_mac_addresses [STRING or FILE] - Extract all the MAC's from a fiven string or File"
  echo "trim            [file]      - Trim leading and trailing spaces (for scripts)"
  echo "get_gpu                     - Pull the GPU Information and Model"
  echo "yt_playlist     [YT-URL]    - Downlaod an entire YT Playlist which will create a directory containing the name of playlist and all videos on chronological order"
  echo "glow_it         [file]      - Render markdown file with Glow"
  echo "show_it         [file]      - Render markdown file with Inlyne a browserless and independent Markdown renderer"
  echo "app_search      [file]      - Search DNF installed app DB for a app/pkg"
  echo "net_up                      - Take control of your services and start your online presence manually"
  echo "wlannet_up                  - Take control of your services and start your wifi online presence manually"
  echo "wlannet_down                               - Take control of your services and stop your wifi presence manually"
  echo "html2mdown [html file] [Md file.md]        - Convert HTML file to Markdown"
  echo "mdown2html  [Md file] [HTML new file.html] - Convert Markdown file to HTML"
  echo "mdown2pdf  [Md file] [PDF file.pdf]        - Convert Markdown file to PDF"
  # echo "ncu -i --format group  - Group packages to update"
  echo ""
  echo " ===================================================================================================="
  tput sgr0
  echo ""
}
