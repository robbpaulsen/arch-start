#!/usr/bin/env bash

read -p "Nombre del proyecto: " proyecto
mkdir ~+/${proyecto}
cd ~+/${proyecto}/
rsync -rzavhP ${HOME}/.local/functions/lib .
touch README.md
touch main.sh
echo -e ''#'/usr/bin/env 'bash'' > ~+/main.sh
