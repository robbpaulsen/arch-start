#!/usr/bin/env bash

######################################################################################################################
#                                                                                                                    #
# Script for a more unattended, headless setup and configurationg for the basic things on any new linux installation.#
#                                                                                                                    #
######################################################################################################################
#tput setaf 0 = black
#tput setaf 1 = red
#tput setaf 2 = green
#tput setaf 3 = yellow
#tput setaf 4 = dark blue
#tput setaf 5 = purple
#tput setaf 6 = cyan
#tput setaf 7 = gray
#tput setaf 8 = light blue
####################################################################################################################

# ----------------------------------------------- Variables ------------------------------------------------------ #
#
uHome=$(basename /home/*)
#
# ----------------------------------------------- Sourcing ------------------------------------------------------ #
#
source ~+/lib/colors/colors.sh
#
# ----------------------------------------------- Funciones ------------------------------------------------------ #
clear

# Detener el script con CTRL + C
ctrl_c() {
  exit
}
trap ctrl_c INT

# Revisar permisos del usuario
if [ "${EUID}" -ne 0 ]; then
  echo -e "\n${YELL}[!]${NC}${BLU} Se te olvido el sudo ...${NC}\n"
  exit
fi

tput setaf 3
echo -e "\t\n[i] Con este script se arreglan la mayoria de los problemas relacionados a una DB corrupta, firmas expiradas y llaves expiradas\nEn una instalacion de cualquier derivada de ArchLinux, si tu problema persiste es hora de leer la ArchWiki ..."
tput sgr0
sleep 3

# Prevenir problemas comunes con pacman, base de datos de las llaves pacman DB y los mirrors de Chaotic y el AUR
## Borrar base actual de Pacman
trashPacmansOldDb() {
  rm -rf /var/lib/pacman/db.lck 2>/dev/null
  rm -rf /var/lib/pacman/sync/* 2>/dev/null
  rm -rf /etc/pacman.d/gnupg/* 2>/dev/null
}

## Reinicializar el llavero de la base de firmas y llaver de Pacman
reInitPacman() {
  pacman-key --init 2>/dev/null &&
    pacman-key --populate 2>/dev/null &&
    pacman -Syy 2>/dev/null
}

usrGnupg() {
  rm -rf /home/${uHome}/.gnupg
  mkdir -p /home/${uHome}/.gnupg
  cp /etc/pacman.d/gnupg/gpg.conf /home/${uHome}/.gnupg/
}

trashPacmansOldDb &&
  usrGnupg &&
  reInitPacman

tput setaf 2
echo -e "\t\n[+] Reinicia tu sistema y Revisa que los errores ya no esten presente, si los errores persisten consultar https://wiki.archlinux.org"
tput sgr0
sleep 3
exit
